# Cryptography I Programming Assignments

This repository contains sample source code for most programming assignments in
the [Cryptography I course on Coursera](https://www.coursera.org/course/crypto).

There are two top-level directories, `haskell` and `rust`. I have implemented
the programming assignments for weeks 2, 3, 4 and 5 in Haskell and for week 3 in
Rust (AES-128-CBC hasn't been implemented in a Rust library AFAICT, so I can't
do the week 2 PA).

## Running

For Haskell, go to the `haskell` folder and run `stack build` Then, run using
`stack exec WeekX`.

For Rust, go to the `rust` folder and from there to the `week-X` folder. Run
`cargo run --release`.
