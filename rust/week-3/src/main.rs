use std::env;
use std::fs::File;
use std::io::Read;

use sodiumoxide::crypto::hash::sha256::{Digest, hash};
use rustc_serialize::hex::ToHex;

const CHUNK_SIZE: usize = 1024;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        panic!("Need exactly 1 argument, the file path");
    }

    let filename = &args[1];

    let mut file = match File::open(filename) {
        Ok(f) => f,
        Err(e) => panic!("Error while opening file: {}", e)
    };

    let mut bytes = Vec::new();
    if let Err(e) = file.read_to_end(&mut bytes) {
        panic!("Error while reading file: {}", e);
    }

    let mut chunks: Vec<&[u8]> = bytes.chunks(CHUNK_SIZE).collect();
    chunks.reverse();

    let checksum: Option<Digest> = chunks.into_iter().fold(None, |acc, elem| {
        match acc {
            None => Some(hash(elem)),
            Some(sum) => Some(hash(&[&elem[..], &sum[..]].concat()[..]))
        }
    });

    let Digest(h) = match checksum {
        Some(c) => c,
        None => panic!("File is empty")
    };

    println!("{}", h.to_hex());
}

extern crate sodiumoxide;
extern crate rustc_serialize;
