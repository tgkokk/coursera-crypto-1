module Week2
  ( main
  ) where

import Util (removePad)

import Crypto.Cipher.AES (AES128)
import qualified Crypto.Cipher.Types as T
import Crypto.Error (throwCryptoError)

import qualified Data.ByteString.Base16 as BSHex
import qualified Data.ByteString.Char8 as BS
import Data.Maybe (fromJust)

mapBoth :: (a -> b) -> (a, a) -> (b, b)
mapBoth f (x, y) = (f x, f y)

blockSize :: Int
blockSize = 16

cbc :: [(BS.ByteString, BS.ByteString)]
cbc =
  map
    (mapBoth BS.pack)
    [ ( "140b41b22a29beb4061bda66b6747e14"
      , "4ca00ff4c898d61e1edbf1800618fb2828a226d160dad07883d04e008a7897ee2e4b7465d5290d0c0e6c6822236e1daafb94ffe0c5da05d9476be028ad7c1d81")
    , ( "140b41b22a29beb4061bda66b6747e14"
      , "5b68629feb8606f9a6667670b75b38a5b4832d0f26e1ab7da33249de7d4afc48e713ac646ace36e872ad5fb8a512428a6e21364b0c374df45503473c5242a253")
    ]

ctr :: [(BS.ByteString, BS.ByteString)]
ctr =
  map
    (mapBoth BS.pack)
    [ ( "36f18357be4dbd77f050515c73fcf9f2"
      , "69dda8455c7dd4254bf353b773304eec0ec7702330098ce7f7520d1cbbb20fc388d1b0adb5054dbd7370849dbf0b88d393f252e764f1f5f7ad97ef79d59ce29f5f51eeca32eabedd9afa9329")
    , ( "36f18357be4dbd77f050515c73fcf9f2"
      , "770b80259ec33beb2561358a9f2dc617e46218c0a53cbeca695ae45faa8952aa0e311bde9d4e01726d3184c34451")
    ]

decrypt ::
     (AES128 -> T.IV AES128 -> BS.ByteString -> BS.ByteString)
  -> BS.ByteString
  -> BS.ByteString
  -> BS.ByteString
decrypt decryptF keyHex cipherTextHex =
  decryptF
    (throwCryptoError $ T.cipherInit key :: AES128)
    (fromJust $ T.makeIV iv)
    encmsg
  where
    key = fst $ BSHex.decode keyHex
    cipherText = fst $ BSHex.decode cipherTextHex
    (iv, encmsg) = BS.splitAt blockSize cipherText

decryptCBC :: BS.ByteString -> BS.ByteString -> BS.ByteString
decryptCBC key ct = removePad $ decrypt T.cbcDecrypt key ct

decryptCTR :: BS.ByteString -> BS.ByteString -> BS.ByteString
decryptCTR = decrypt T.ctrCombine

main :: IO ()
main =
  mapM_ (putStrLn . BS.unpack) $
  (map (uncurry decryptCBC) cbc) ++ (map (uncurry decryptCTR) ctr)
