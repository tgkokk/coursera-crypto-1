{-# LANGUAGE DataKinds, TypeOperators #-}

module Week5
  ( main
  ) where

import Data.Modular

import qualified Data.Map.Lazy as M

type Val
   = ℤ / 13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084171

type Map = M.Map Val Integer

g :: Val
g =
  11717829880366207009516117596335367088558084999998952205599979459063929499736583746670572176471460312928594829675428279466566527115212748467589894601965568

h :: Val
h =
  3239475104050450443565264378728065788649097520952449527834792452971981976143292558073856937958553180532878928001494706097394108577585732452307673444020333

b :: Integer
b = 2 ^ 20

invg :: Val
invg = inv g

gToB :: Val
gToB = g ^ b

addToMap :: Map
addToMap = fst $ foldl addToMap' (M.insert h 0 M.empty, h) [1 .. b]
  where
    addToMap' :: (Map, Val) -> Integer -> (Map, Val)
    addToMap' (m, p) x = (M.insert newValue x m, newValue)
      where
        newValue = p * invg

findColl :: Map -> Maybe Integer
findColl m = findColl' 1 0
  where
    findColl' c x
      | x >= b = Nothing
      | otherwise =
        case M.lookup c m of
          Just x' -> Just $ x * b + x'
          Nothing -> findColl' (c * gToB) (x + 1)

solveDLog :: Maybe Integer
solveDLog = findColl addToMap

main :: IO ()
main = print solveDLog
