module Week3
  ( main
  ) where

import Util (splitTo)

import Data.Maybe (fromMaybe)

import qualified Crypto.Hash.SHA256 as SHA256

import qualified Data.ByteString as BS
import qualified Data.ByteString.Base16 as BSHex
import qualified Data.ByteString.Char8 as BSChar

import System.Environment (getArgs)

chunkSize :: Int
chunkSize = 1024

process :: BS.ByteString -> Maybe BS.ByteString
process x =
  foldl
    (\acc chunk -> Just $ SHA256.hash $ maybe chunk (BS.append chunk) acc)
    Nothing
    reversed
  where
    split = splitTo chunkSize x
    reversed = reverse split

main :: IO ()
main = do
  args <- getArgs
  if length args /= 1
    then error "Need exactly 1 argument, the file path"
    else do
      contents <- BS.readFile file
      putStrLn $
        fromMaybe "File is empty" $
        (BSChar.unpack . BSHex.encode) <$> process contents
  where
    file = head args
