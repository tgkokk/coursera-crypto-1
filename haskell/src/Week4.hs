module Week4
  ( main
  ) where

import Util (splitTo)

import Data.Bits (xor)
import Data.Char (chr, ord)
import Data.List (find)
import Data.Maybe (fromJust)
import Data.Word (Word8)

import Control.Concurrent.ParallelIO.Global (parallel)
import Control.Monad (foldM, liftM)

import qualified Data.ByteString as BS
import qualified Data.ByteString.Base16 as BSHex
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as BSL

import Network.HTTP.Client (defaultManagerSettings)
import Network.HTTP.Conduit
import Network.HTTP.Types (Status(..))

blockSize :: Int
blockSize = 16

validLetters :: [Int]
validLetters = ord ' ' : [ord 'a' .. ord 'z'] ++ [ord 'A' .. ord 'Z']

validPads :: [Int]
validPads = [1 .. blockSize]

ct :: BSC.ByteString
ct =
  BSC.pack
    "f20bdba6ff29eed7b046d1df9fb7000058b1ffb4210a580f748b4ac714c001bd4a61044426fb515dad3f21f18aa577c0bdf302936266926ff37dbf7035d5eeb4"

website :: BSC.ByteString
website = BSC.pack "http://crypto-class.appspot.com/po?er="

request :: Manager -> String -> IO (Response BSL.ByteString)
request mgr url = do
  req <- parseRequest url
  httpLbs req mgr

isValid :: Manager -> BS.ByteString -> IO Bool
isValid mgr ct = do
  req <- request mgr $ BSC.unpack $ BS.append website $ BSHex.encode ct
  return $
    case responseStatus req of
      Status 404 _ -> True
      _ -> False

xorPad :: Int -> BS.ByteString -> BS.ByteString
xorPad p bs = BS.append l $ BS.map (`xor` (fromIntegral p :: Word8)) r
  where
    (l, r) = BS.splitAt (blockSize - p) bs

xorGuess :: Int -> BS.ByteString -> Int -> BS.ByteString
xorGuess p bs g =
  BS.append l $ BS.cons (BS.head r `xor` (fromIntegral g :: Word8)) $ BS.tail r
  where
    (l, r) = BS.splitAt (blockSize - p) bs

findValid :: Manager -> BS.ByteString -> BS.ByteString -> Int -> [Int] -> IO Int
findValid mgr l r p validValues =
  liftM (snd . fromJust . find fst) $
  parallel $
  map
    (\g -> do
       valid <- isValid mgr (BS.append (xorGuess p l g) r)
       return (valid, g))
    validValues

findPlaintextChar ::
     Manager
  -> BS.ByteString
  -> BS.ByteString
  -> BS.ByteString
  -> Int
  -> IO (BS.ByteString, BS.ByteString)
findPlaintextChar mgr acc l r p = do
  g <- findValid mgr (xorPad p l) r p validLetters
  putStrLn $
    "The char at pos " ++ show (blockSize - p) ++ " is " ++ show (chr g)
  return (BSC.cons (chr g) acc, xorGuess p l g)

skipPadding :: Manager -> BS.ByteString -> BS.ByteString -> IO Int
skipPadding mgr l r =
  liftM (snd . fromJust . find fst) $
  parallel $
  map
    (\g -> do
       valid <- isValid mgr (BS.append (xorPad g l) r)
       return (valid, g))
    validPads

findPlaintext ::
     Manager -> BS.ByteString -> BS.ByteString -> Bool -> IO BS.ByteString
findPlaintext mgr l r isLast = do
  pad <-
    if isLast
      then liftM (+ 1) $ skipPadding mgr l r
      else return 0
  liftM fst $
    foldM
      (\(bs, l') p -> findPlaintextChar mgr bs l' r p)
      (BS.empty, BS.pack $ BS.zipWith xor dummy l)
      [(if isLast
          then pad + 1
          else 1) .. 16]
  where
    dummy =
      BS.append
        (BS.replicate (blockSize - pad) 0)
        (BS.replicate pad $ fromIntegral pad)

findPlaintexts :: Manager -> IO BS.ByteString
findPlaintexts mgr =
  liftM BS.concat $
  parallel $
  map (uncurry3 $ findPlaintext mgr) $ pairs $ splitTo blockSize ctDec
  where
    ctDec = fst $ BSHex.decode ct

pairs :: [a] -> [(a, a, Bool)]
pairs xs = f $ zip xs (tail xs)
  where
    f xs =
      map (\(a, b) -> (a, b, False)) (init xs) ++
      [(\(a, b) -> (a, b, True)) (last xs)]

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c

main :: IO ()
main = do
  mgr <- newManager defaultManagerSettings
  liftM BSC.unpack (findPlaintexts mgr) >>= putStrLn
