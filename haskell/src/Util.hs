module Util
  ( splitTo
  , removePad
  ) where

import qualified Data.ByteString as BS

removePad :: BS.ByteString -> BS.ByteString
removePad bs = BS.take (len - last) bs
  where
    len = BS.length bs
    last = fromIntegral $ BS.last bs

splitTo :: Int -> BS.ByteString -> [BS.ByteString]
splitTo x s
  | s == BS.empty = []
  | otherwise = l : splitTo x r
  where
    (l, r) = BS.splitAt x s
